#include "grammar.h"


Grammar::Grammar()
{
}

Grammar::~Grammar()
{
	delete[] sChainlet;
}

int Grammar::build()
{
	input();
	char* result = checkRuleS(sChainlet);
	if (result == nullptr || *result)
	{
		cout << "Error: constant was rejected";
		return 1;
	}
	else
	{
		printRules();
		return 0;
	}
}

void Grammar::input()
{
	cout << "Enter constant:";
	cin >> sChainlet;
}

void Grammar::printRules() {
	int i = 0, num = 0;
	cout << "Rule";
	while ((num = nRuleNumber[i]) != NULL)
	{
		cout << " -> " << grammary[num - 1];
		i++;
	}
}

void Grammar::setRule(int _ruleNumber)
{
	if (nAmount < 256)
	{
		nRuleNumber[nAmount] = _ruleNumber;
		nAmount++;
	}
	else
	{
		cout << "Error: entered constant to long";
	}
}

char* Grammar::checkRuleS(char* _sChainlet)
{
	switch (*_sChainlet) {
	case '\0':
		return _sChainlet;
	case 'a':
		setRule(1);
		return checkRuleA(_sChainlet + 1);
		break;
	case 'b':
		setRule(2);
		return checkRuleB(_sChainlet + 1);
		break;
	default:
		return NULL;
	}
}

char* Grammar::checkRuleA(char* _sChainlet)
{
	switch (*_sChainlet) {
	case '\0':
		return _sChainlet;
	case 'a':
		setRule(3);
		return checkRuleB(_sChainlet + 1);
		break;
	case 'd':
		setRule(4);
		return checkRuleB(_sChainlet + 1);
		break;
	case 'c':
		setRule(5);
		return _sChainlet + 1;
		break;
	default:
		return NULL;
	}
}

char* Grammar::checkRuleB(char* _sChainlet)
{
	switch (*_sChainlet) {
	case '\0':
		return _sChainlet;
	case 'b':
		setRule(6);
		return checkRuleA(_sChainlet + 1);
		break;
	case 'c':
		setRule(7);
		return checkRuleC(_sChainlet + 1);
		break;
	default:
		return NULL;
	}
}

char* Grammar::checkRuleC(char* _sChainlet)
{
	if (*_sChainlet == 'b')
	{
		setRule(9);
		return checkRuleB(_sChainlet + 1);
	}
	else if (*_sChainlet == 'd' && *(_sChainlet + 1) == '\0')
	{
		setRule(10);
		return _sChainlet + 1;
	}
	else if (*_sChainlet == 'd')
	{
		setRule(8);
		return checkRuleA(_sChainlet + 1);
	}
	else
	{
		return nullptr;
	}
}
