#pragma once
#include <iostream>

using namespace std;

class Grammar
{
public:
	Grammar();

	~Grammar();

	int build();

private:
	char* sChainlet = new char[256];
	int nRuleNumber[256];
	const char* grammary[10] { "aA", "bB", "aB", "dB", "c", "bA", "cC", "dA", "bB", "d" };
	int nAmount = 0;

	void input();

	void printRules();

	void setRule(int _ruleNumber);

	char* checkRuleS(char* _sChainlet);

	char* checkRuleA(char* _sChainlet);

	char* checkRuleB(char* _sChainlet);

	char* checkRuleC(char* _sChainlet);
};

