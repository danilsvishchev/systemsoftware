﻿// SPO5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include "grammar.h"

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "Russian");
	Grammar* grammar = new Grammar();
	int exit = grammar->build();
	delete grammar;
	_CrtDumpMemoryLeaks();
	return exit;
}
